package stepDefinitions;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Então;
import org.apache.http.HttpStatus;
import org.json.simple.JSONObject;

import static io.restassured.RestAssured.delete;
import static io.restassured.RestAssured.given;

public class RemoverSimulacaoStepDef {

    private static final String put = "/simulacoes";

    @Dado("que o usuario insere {int}")
    public void queOUsuarioInsere(int ID) {
        given().
        when().
                delete(delete() + "/" + ID);

    }

    @Então("e retornado que a simulacao foi removida com sucesso")
    public void e_retornado_que_a_simulacao_foi_removida_com_sucesso() throws Exception {
        given().
                then().
                statusCode(HttpStatus.SC_NO_CONTENT);

    }

    @Então("e retornado que a simulacao nao foi encontrada")
    public void e_retornado_que_a_simulacao_nao_foi_encontrada() throws Exception {
        given().
                then().
                statusCode(HttpStatus.SC_NOT_FOUND);

    }

}
