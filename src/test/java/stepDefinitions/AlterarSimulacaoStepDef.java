package stepDefinitions;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Então;
import org.apache.http.HttpStatus;
import org.json.simple.JSONObject;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;


public class AlterarSimulacaoStepDef {

    private static final String put = "/simulacoes";

    @Dado("altera o dado {string} do {string}")
    public void altera_o_dado(String nome, String CPF) throws Exception {
        JSONObject requestParams = new JSONObject();
        requestParams.put("nome", nome);

        given().
                body(requestParams.toJSONString()).
                when().
                put(put + "/" + CPF);

    }

    @Então("e retornado o sucesso da operacao")
    public void e_retornado_o_sucesso_da_operacao() throws Exception {
        given().
                then().
                statusCode(HttpStatus.SC_OK);

    }

    @Então("e retornado a mensagem de CPF nao encontrado")
    public void e_retornado_a_mensagem_de_cpf_nao_encontrado() throws Exception {
        given().
                then().
                statusCode(HttpStatus.SC_NOT_FOUND);

    }

}
