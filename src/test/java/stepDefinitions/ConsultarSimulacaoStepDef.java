package stepDefinitions;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Então;
import io.restassured.http.ContentType;
import org.apache.http.HttpStatus;

import static io.restassured.RestAssured.given;

public class ConsultarSimulacaoStepDef {

    private static final String get = "/simulacoes";

    @Dado("que o usuario consulta uma simulacao")
    public void queOUsuarioConsultaUmaSimulacao() throws Exception {
        given().
                when().
                get(get);

    }

    @Entao("é retornado uma lista de simulacoes")
    public void é_retornado_uma_lista_de_simulacoes() throws Exception {
        given().
                then().
                statusCode(HttpStatus.SC_OK);
    }

    @Então("é retornado que existe uma simulacao cadastrada")
    public void éRetornadoQueExisteUmaSimulacaoCadastrada() throws Exception {
        given().
                then().
                statusCode(HttpStatus.SC_OK);

    }

    @Então("e retornado que nao existe simulacao cadastrada")
    public void eRetornadoQueNaoExisteSimulacaoCadastrada() {
        given().
                then().
                statusCode(HttpStatus.SC_NO_CONTENT);
    }

    @Então("e retornado que nao existe simulacao cadastrada para o CPF")
    public void eRetornadoQueNaoExisteSimulacaoCadastradaParaOCPF() {
        given().
                then().
                statusCode(HttpStatus.SC_NOT_FOUND);
    }
}
