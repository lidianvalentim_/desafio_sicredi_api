package stepDefinitions;

import Domain.User;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Então;
import org.apache.http.HttpStatus;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;

public class CriarSimulacaoStepDef {

    private static final String post = "/simulacoes";

    @Dado("que o usuario insere {string}, {string}, {string}, {string}, {string} e {string}")
    public void que_o_usuario_insere_e(String nome, String CPF, String email, String valor, String parcelas, String seguro) throws Exception {
        User user = new User(nome, CPF, email, valor, parcelas, seguro);

        given().
                body(user).
                when().
                post(post);
    }

    @Então("é retornado o sucesso da simulacao")
    public void é_retornado_o_sucesso_da_simulacao() throws Exception {
        given().
                then().
                statusCode(HttpStatus.SC_CREATED);
        ;
    }

    @Então("e retornado uma lista de erros")
    public void eRetornadoUmaListaDeErros() throws Exception {
        given().
                then().
                statusCode(HttpStatus.SC_BAD_REQUEST).
                body("erros", containsString("não pode ser vazio"));
    }

    @Então("e retornado uma mensagem de CPF existente")
    public void eRetornadoUmaMensagemDeCPFExistente() throws Exception {
        given().
                then().
                statusCode(HttpStatus.SC_CONFLICT).
                body("mensagem", is("CPF duplicado"));
    }
}
