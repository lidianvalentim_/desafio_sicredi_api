package stepDefinitions;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Então;
import io.restassured.http.ContentType;
import org.apache.http.HttpStatus;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;

public class ConsultarRestricaoCPFStepDef {

    private static final String get = "/restricoes";

    @Dado("que o usuário insere um {string}")
    public void queOUsuárioInsereUm(String CPF) {
        given().
                contentType(ContentType.ANY).
                when().
                get(get + "/" + CPF);
    }

    @Então("e retornado o response de CPF sem restricao.")
    public void e_retornado_o_response_de_cpf_sem_restricao() throws Exception {
        given().
                then().
                statusCode(HttpStatus.SC_NO_CONTENT);

    }

    @Então("e retornado o response de CPF com restricao.")
    public void eRetornadoOResponseDeCPFComRestricao() {
        given().
                then().
                statusCode(HttpStatus.SC_OK).
                body("mensagem", containsString("o cpf " + "" + "tem problema"));


    }

}
