#language: pt
Funcionalidade: Fazer uma alteracao de CPF

  Como um usuario gostaria de alterar um CPF ja existente e verificar o seu retorno

  @AlteracaoSucesso @AlterarSimulacao @TotalTest
  Esquema do Cenário: Alterar um CPF existente
    Dado que o usuário insere um "<CPF>"
    E altera o dado "<nome>" do "<CPF>"
    Então e retornado o sucesso da operacao
    Exemplos:
      | nome             | CPF         |
      | Lidiane Valentim | 97093236014 |

  @AlteracaoCPFNaoEncontrado @AlterarSimulacao @TotalTest
  Esquema do Cenário: Alterar um CPF nao encontrado
    Dado que o usuário insere um "<CPF>"
    E altera o dado "<nome>" do "<CPF>"
    Então e retornado a mensagem de CPF nao encontrado
    Exemplos:
      | nome             | CPF         |
      | Lidiane Valentim | 97093236014 |

