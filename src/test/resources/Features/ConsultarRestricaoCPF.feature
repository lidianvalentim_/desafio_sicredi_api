#language: pt
Funcionalidade: Consultar restrição de CPF e verificar o retorno

  Como usuário gostaria de submeter request para o webservice Via CPF
  Para consultar se possui restricoes

  @CPFSemRestricao @ConsultaRestricao @TotalTest
  Esquema do Cenário: Consultar CPF sem restricao
    Dado que o usuário insere um "<CPF>"
    Então e retornado o response de CPF sem restricao.
    Exemplos:
      | CPF         |
      | 01302203314 |

  @CPFComRestricao @ConsultaRestricao @TotalTest
  Esquema do Cenário: Consultar CPF com restricao
    Dado que o usuário insere um "<CPF>"
    Então e retornado o response de CPF com restricao.
    Exemplos:
      | CPF         |
      | 60094146012 |



