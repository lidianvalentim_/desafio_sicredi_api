#language: pt
Funcionalidade: Remover simulacoes de CPF's cadastrados

  Como um usuario gostaria de remover um ou mais simulacoes de CPF's cadastrados

  @RemoverSimulacao @RemoverSimulacaoSucesso @TotalTest
  Esquema do Cenário: Remover uma simulacao com sucesso
    Dado que o usuario insere <ID>
    Entao e retornado que a simulacao foi removida com sucesso
    Exemplos:
    |ID|
    | 14 |

  @RemoverSimulacao @RemoverSimulacaoNaoEncontrada @TotalTest
  Esquema do Cenário: Remover uma simulacao nao encontrada
    Dado que o usuario insere <ID>
    Entao e retornado que a simulacao nao foi encontrada
    Exemplos:
      |ID|
      |14  |
