#language: pt
Funcionalidade: Consultar todas as simulacoes de CPF's cadastrados

  Como um usuario gostaria de consultar todas as simulacoes cadastradas

  @ConsultarSimulacao @ListaEncontrada @TotalTest
  Cenário: Consultar lista de simulacoes
    Dado que o usuario consulta uma simulacao
    Então é retornado uma lista de simulacoes

  @ConsultarSimulacao @ListaNaoEncontrada @TotalTest
  Cenário: Consultar lista de simulacoes nao existente
    Dado que o usuario consulta uma simulacao
    Então e retornado que nao existe simulacao cadastrada

  @ConsultarSimulacao @CPFEncontrado @TotalTest
  Esquema do Cenário: Consultar uma simulacao de CPF encontrado
    Dado que o usuário insere um "<CPF>"
    Então é retornado que existe uma simulacao cadastrada
    Exemplos:
      | CPF |
      | 17822386034 |

  @ConsultarSimulacao @CPFNãoEncontrado @TotalTest
  Esquema do Cenário: Consultar uma simulacao de CPF não encontrado
    Dado que o usuário insere um "<CPF>"
    Então e retornado que nao existe simulacao cadastrada para o CPF
    Exemplos:
      | CPF |
      | 00988766556    |

