#language: pt
Funcionalidade: Criar uma simulação para CPF

  Como um usuário gostaria de criar uma simulação de cadastro para um CPF e verificar o retorno

  @CriarSimulacaoSucesso @CriarSimulacao @TotalTest
  Esquema do Cenário: Criar uma simulacao com sucesso
    Dado que o usuario insere "<nome>", "<CPF>", "<email>", "<valor>", "<parcelas>" e "<seguro>"
    Então é retornado o sucesso da simulacao
    Exemplos:
      | nome             | CPF         | email                       | valor | parcelas | seguro |
      | Lidiane Valentim | 08855433212 | lidianevalentim@hotmail.com | 1400  | 4        | true   |

  @CriarSimulacaoErro @CriarSimulacao @TotalTest
  Esquema do Cenário: Criar uma simulação com problema em alguma regra
    Dado que o usuario insere "<nome>", "<CPF>", "<email>", "<valor>", "<parcelas>" e "<seguro>"
    Então e retornado uma lista de erros
    Exemplos:
      | nome             | CPF         | email | valor | parcelas | seguro |
      | Lidiane Valentim | 97093236014 |       |       |          | true   |

  @CriarSimulacaoExistente @CriarSimulacao @TotalTest
  Esquema do Cenário: Criar uma simulacao para CPF ja existente
    Dado que o usuario insere "<nome>", "<CPF>", "<email>", "<valor>", "<parcelas>" e "<seguro>"
    Então e retornado uma mensagem de CPF existente
    Exemplos:
      | nome             | CPF         | email             | valor | parcelas | seguro |
      | Lidiane Valentim | 97093236014 | dummy@hotmail.com | 1200  | 3        | true   |